<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Node;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

   public function save_user(Request $request){

	 $this->validate($request, [
     'job_title' => 'required|string|max:255',
     'name' => 'required|string|max:255',
     'email' => 'required|string|email|max:255|unique:nodes',
     'password' => 'required|string|min:6',
   ]);

    $task = new Node;
    $task->name = $request->name;
    $task->email = $request->email;
    $task->job_title = $request->phone;
    $task->password =$request->password;
    $task->save();
    }


}
