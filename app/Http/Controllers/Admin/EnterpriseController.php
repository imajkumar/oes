<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
class EnterpriseController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    public function index()
    {

        return view('admin.enterprise');
    }
    public function addmember(){
      $users = Admin::all();
          return response()->json([
              'users' => $users
          ]);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email'
        ]);
        $user = Admin::create([
            'username' => $request->input('username'),
            'email' => $request->input('email')
        ]);
        return response()->json([
            'message' => 'user created successfully',
            'user' => $user
        ]);
    }


    public function enterpriselist()
    {
        return view('admin.enterpriselist');
    }



}
