@extends('admin.layouts.adminlayout')
@section('pageTitle', 'Dashboard')

@section('content')
<div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="file-manager">

                                <h5>Enterprise Menu</h5>
                                <ul class="folder-list" style="padding: 0">
                                    <li><a href="/admin/enterprise/list"><i class="fa fa-folder"></i> List</a></li>

                                </ul>


                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 animated fadeInRight">
                    <div class="row">
                      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Basic form <small>Simple login form example</small></h5>
                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>



                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row">
                          <div class="col-sm-12 b-r"><h3 class="m-t-none m-b">Sign in</h3>
                              <p>Sign in today for more expirience.</p>
@if (count($errors) > 0)
   <div class = "alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
@endif

			 <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.save.submit') }}">
                        {{ csrf_field() }} 
				<input type="text" name="name" placeholder="First name">
				<input type="email" name="email" placeholder="email">
				<input type="text" name="phone" placeholder="phone">
				<input type="password" name="password" placeholder="password">
				<button type="submit" class="btn btn-primary">
                                    submit
                                </button>


			</form>
                              

                          </div>

                      </div>
                  </div>
              </div>
          </div>
                    </div>
                    </div>
                </div>
                </div>
@endsection
