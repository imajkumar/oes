<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index');

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/enterprise', 'Admin\EnterpriseController@index')->name('admin.enterprise');
    Route::get('/enterprise/list', 'Admin\EnterpriseController@enterpriselist')->name('admin.enterprise.list');
     Route::post('/save_user', 'AdminController@save_user')->name('admin.save.submit');
  });
  Route::prefix('member')->group(function() {
      Route::get('/login', 'Auth\MemberLoginController@showLoginForm')->name('member.login');
      Route::post('/login', 'Auth\MemberLoginController@login')->name('member.login.submit');
      Route::get('/', 'MemberController@index')->name('member.dashboard');
    });
    Route::prefix('node')->group(function() {
        Route::get('/login', 'Auth\NodeLoginController@showLoginForm')->name('node.login');
        Route::post('/login', 'Auth\NodeLoginController@login')->name('node.login.submit');
        Route::get('/', 'NodeController@index')->name('node.dashboard');
  });
